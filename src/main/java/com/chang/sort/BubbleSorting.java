package com.chang.sort;

/**
 * 冒泡排序  O(n^2)
 */
public class BubbleSorting {

    public static void main(String[] args) {

        int[] array = {3,-1,5,2,10,20};

        int temp = 0;

        for (int i = 0; i < array.length - 1; i++) {
            boolean flag = false;
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j+1]){
                    temp = array[j+1];
                    array[j+1] = array[j];
                    array[j] = temp;
                    flag = true;
                }
            }
            if (!flag){
                break;
            }
        }
        for (int i : array) {
            System.out.print(i);
        }
    }
}
