package com.chang.sort;

import java.util.Arrays;

/**
 *  插入排序
 *
 *  把待排序的元素 看成一个有序表和一个无序表，排序的过程中每次从无序表中取出第一个元素，将其与有序表元素依次比较，
 *  将其插入有序表中的适当位置，使之成为新的有序表。
 */
public class InsertionSorting {

    public static void main(String[] args) {
        int[] array = {1,3,2,0,-1,9,10,7,-3};
        insertSort(array);
        System.out.println(Arrays.toString(array));
    }

    public static void insertSort(int[] array){

        // 从1开始 0当作一个有序列表 不用排序
        for (int i = 1; i < array.length; i++) {
            //  {1,3,2,0};
            // 待插入数据
            int insertValue = array[i];
            // 待插入索引  即array[1]的前一个元素的索引
            int insertIndex = i - 1;

            // insertIndex >= 0 该判断条件 避免越界
            // insertValue < array[insertIndex] 该判断条件满足 说明还没有找到位置 即需要将array[insertIndex] 后移
            while (insertIndex >= 0 && insertValue < array[insertIndex]){
                // 元素后移
                array[insertIndex + 1] = array[insertIndex];

                insertIndex --;
            }
            // 退出循环说明插入的位置找到
            array[insertIndex + 1] = insertValue;
        }
    }
}
