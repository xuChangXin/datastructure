package com.chang.sort;

/**
 * 选择排序 时间复杂度 O(n^2)
 * 第一次从array[0] ~ array[n-1] 中找出最小的元素，然后与array[0] 进行交换；
 *
 * 第二次从array[1] 开始 ~ array[n-1] 中找出最小的元素，然后与array[1] 进行交换 ；
 *
 * 第三次从array[2] 开始 ~ array[n-1] 中找出最小的元素，然后与array[2] 进行交换 ；
 *
 * 以此类推 进行 n-1 次操作，就可以的到一个从小到大排序的有序序列
 *
 * 如若查询从大到小 反之；（第一次从array[0] ~ array[n-1] 中找出最大的元素，然后与array[0] 进行交换；）
 */
public class SelectSorting {

    public static void main(String[] args) {
        int[] array = {3, -1, 5, 2, 10, 20, 9, -1, 20, 3, 1, 100, 77, 66, 88, -100};

        int index = 0;
        int min = 0;
        int minIndex = 0;
        for (int i = 0; i < array.length - 1; i++) {
            min = array[index];
            minIndex = index;
            for (int j = index; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minIndex = j;
                }
            }

            if (index != minIndex) { // 交换操作
                int temp = array[index];
                array[index] = min;
                array[minIndex] = temp;
            }
            index++;
        }

        for (int j : array) {
            System.out.println(j);
        }
    }
}
