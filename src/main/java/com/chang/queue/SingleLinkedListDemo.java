package com.chang.queue;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class SingleLinkedListDemo {

    public static void main(String[] args) {
        HeroNode heroNode1 = new HeroNode(1, "宋江", "及时雨");
        HeroNode heroNode2 = new HeroNode(2, "李奎", "黑旋风");
        HeroNode heroNode3 = new HeroNode(3, "林冲", "豹子头");
        HeroNode heroNode4 = new HeroNode(4, "吴用", "智多星");
        SingleLinkedList singleLinkedList = new SingleLinkedList();
//        singleLinkedList.addHeroNode(heroNode1);
//        singleLinkedList.addHeroNode(heroNode2);
//        singleLinkedList.addHeroNode(heroNode3);
//        singleLinkedList.addHeroNode(heroNode4);

        // 插入指定位置
        singleLinkedList.addHeroNodeOrderByNo(heroNode1);
        singleLinkedList.addHeroNodeOrderByNo(heroNode3);
        singleLinkedList.addHeroNodeOrderByNo(heroNode2);
        singleLinkedList.addHeroNodeOrderByNo(heroNode4);

        singleLinkedList.showNode();

        HeroNode heroNode4b = new HeroNode(4, "武松", "行者");
        singleLinkedList.updateNode(heroNode4b);
        System.out.println("update======");
        singleLinkedList.showNode();

        System.out.println("delete=======");
        HeroNode heroNode2b = new HeroNode(2, "武松", "行者");
        singleLinkedList.delNode(heroNode2b);
        singleLinkedList.showNode();

        int length = singleLinkedList.getLength(singleLinkedList.getHeadNode());
        System.out.println("node length "+length);

        HeroNode heroNode = singleLinkedList.getHeroNode(singleLinkedList.getHeadNode(), 4);
        System.out.println(heroNode);


        System.out.println("链表反转-----------------------");
        // 链表反转
        singleLinkedList.reverseNode();
        singleLinkedList.showNode();

        System.out.println("逆序打印-----------------------");
        // 逆序打印
        singleLinkedList.reversePrint();


        singleLinkedList.sortLinkedList(singleLinkedList.getHeadNode(),singleLinkedList.getHeadNode());
    }
}


class SingleLinkedList {

    // 头节点 不存放数据
    private HeroNode headNode = new HeroNode(0,"","");


    HeroNode getHeadNode(){
        return headNode;
    }

    // 添加结点
    public void  addHeroNode(HeroNode heroNode){
        HeroNode node = headNode;
        while (true){
            if (node.next == null){
                break;
            }
            node = node.next;
        }
        node.next = heroNode;
    }

    // 添加结点 根据no排序
    public void  addHeroNodeOrderByNo(HeroNode heroNode){
        HeroNode temp = headNode;

        boolean flag = false;

        while (true){
            // 判断临时变量 temp的next域是否为空 就直接添加
            if (temp.next == null){
                break;
            }
            // 判断next域中对象的编号是否大于准备存入的对象编号 大于的话直接插入temp之后
            if (temp.next.no > heroNode.no){
                break;
            }
            if (temp.next.no.equals(heroNode.no)){
                flag = true;
                break;
            }
            temp = temp.next;
        }
        if (flag){
            throw new RuntimeException("该位置元素已经存在 插入的编号为 "+heroNode.no);
        }else {
            heroNode.next = temp.next;
            temp.next = heroNode;
        }
    }

    public void updateNode(HeroNode newHeroNode){
        if (headNode.next == null){
            throw new RuntimeException("链表为空");
        }
        HeroNode temp = headNode.next;

        while (true){
            if (temp == null){
                throw new RuntimeException("没有找到编号为 "+newHeroNode.no+"的结点");
            }
            if (temp.no.equals(newHeroNode.no)){
                temp.name = newHeroNode.name;
                temp.nickName = newHeroNode.nickName;
                break;
            }
            temp = temp.next;
        }
    }

    public void delNode(HeroNode delHeroNode){
        if (headNode.next == null){
            System.out.println("链表为空");
            return;
        }
        HeroNode temp = headNode;
        while (true){
            if (temp.next == null){
                break;
            }
            if (temp.next.no.equals(delHeroNode.no)){
                temp.next = temp.next.next;
                break;
            }
            temp = temp.next;
        }
    }

    public void showNode(){
        if (headNode.next == null){
            System.out.println("链表为空");
            return;
        }
        HeroNode node = headNode.next;
        while (true){
            if (node == null){
                break;
            }
            System.out.println(node);
            node = node.next;
        }
    }

    public int getLength(HeroNode head){
        if (head.next == null){
            return 0;
        }
        int length = 0;
        HeroNode cur = head.next;
        while (cur != null){
            length++;
            cur = cur.next;
        }
        return length;
    }

    public HeroNode getHeroNode(HeroNode headNode,Integer index){
        int length = getLength(headNode);

        HeroNode temp = headNode;
        int num = 0;
        while (true){
            if (temp.next == null){
                break;
            }
            if (num == length-index){
                break;
            }
            num++;
            temp = temp.next;
        }
        return temp.next;
    }

    /**
     * 单链表反转
     */
    public void reverseNode(){
        if (headNode.next == null || headNode.next.next == null){
            System.out.println("暂无结点或无需反转");
            return;
        }
        // 定义一个新的头节点
        HeroNode newHeadNode = new HeroNode(0, "", "");

        // 存储第一个结点
        HeroNode cur = headNode.next;

        // 辅助变量 在上一个结点断开前赋值给该变量 避免结点断开后找不到节点
        HeroNode next = null;

        while (cur != null){
            // 将当前结点的下一个结点赋值给 临时变量
            next = cur.next;
            // 将当前结点的下一个结点设置为 newHeadNode.next  上一个赋值操作就是将原有的后续链表保持下来 赋值到临时变量(next)存储
            // cur.next = newHeadNode.next 会将newHeadNode.next的值给到当前结点的next 这样原有的后续数据就被替换掉为newHeadNode的next数据
            cur.next = newHeadNode.next;
            // 上一步操作仅仅是将 newHeadNode的next值赋值给了当前结点
            // 但是newHeadNode 没和当前结点有关联 所以newHeadNode.next = cur 将当前结点(cur)赋值给newHeadNode的next中
            newHeadNode.next = cur;

            // 上面操作完后 cur已经转移到newHeadNode的next中了 cur就没有作用了 需要将之前临时变量(next)赋值给cur 因为next中存储的是当前cur后的原有链表数据
            // 再进行新的一轮反转
            cur = next;
        }
        // 反转完后 newHeadNode中是反转后的数据 只需将原有的headNode.next 指向 newHeadNode.next 这样headNode中就是反转后的数据
        headNode.next = newHeadNode.next;
    }

    /**
     * 逆序打印
     */
    public void reversePrint(){

        if (headNode.next == null){
            return;
        }

        Stack<HeroNode> heroNodeStack = new Stack<>();

        HeroNode temp = headNode;
        while (temp.next != null){
            heroNodeStack.add(temp.next);
            temp = temp.next;
        }

        while (heroNodeStack.size()>0){
            System.out.println(heroNodeStack.pop());
        }
    }

    public void sortLinkedList(HeroNode headNodeOne,HeroNode headNodeTwo){

        HeroNode temp = headNodeOne;

        while (temp.next != null){
            while (true){
                HeroNode temp2 = headNodeTwo;
                HeroNode temp3 = null;
                if (temp.next.no > temp2.next.no){
                    temp3 = temp2.next;
                    temp2.next = temp.next;
                    temp.next = temp3;
                    break;
                }else {
                   temp3 = temp2.next;
                   temp2.next = temp.next;
                   temp.next = temp3;
                   break;
                }
            }
            temp = temp.next;
        }
        System.out.println("------------");
    }
}

class HeroNode{
    public Integer no;
    public String name;
    public String nickName;
    public HeroNode next;

    public HeroNode(Integer no,String name,String nickName){
        this.no = no;
        this.name = name;
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "HeroNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", nickName='" + nickName + '\'' +
                '}';
    }
}