package com.chang.queue;

import java.util.Scanner;

public class ArrayQueue {
}

class Queue{
    // 最大容量
    private int maxSize;
    // 队列头
    private int front;
    // 队列尾
    private int rear;
    // 数据存放
    private int[] arr;

    public Queue(int arrayMaxSize){
        maxSize = arrayMaxSize;
        arr = new int[maxSize];
        // 队列头部 指向队列头部的前一个位置
        front = -1;
        // 队列尾部 指向队列的最后一个数据
        rear = -1;
    }

    /**
     * 判断队列是否满了
     * @return
     */
    public Boolean isFull(){
        return  rear == maxSize - 1;
    }

    /**
     * 判断队列为空
     * @return
     */
    public Boolean isEmpty(){
        return rear == front;
    }

    public void addQueue(int num){
        if (isFull()){
            System.out.println("队列已满,无法存入");
            return;
        }
        rear++;
        arr[rear] = num;
    }

    public int getQueue(){
        if (isEmpty()){
            System.out.println("暂无数据!");
            throw new RuntimeException("队列为空,暂无数据!");
        }
        front++;
        return arr[front];
    }

    public void showQueue(){
        if (isEmpty()){
            System.out.println("队列为空,暂无数据");
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.printf("array[%d]=[%d]\n",i,arr[i]);

        }
    }

    public int showFront(){
        if (isEmpty()){
            System.out.println("队列为空,暂无数据");
            throw new RuntimeException("队列为空,暂无数据!");
        }
        System.out.printf("front=%d\n",arr[front + 1]);
        return arr[front + 1];
    }

    public int showRear(){
        if (isEmpty()){
            System.out.println("队列为空,暂无数据");
            throw new RuntimeException("队列为空,暂无数据!");
        }
        System.out.printf("rear=%d\n",arr[rear]);
        return arr[rear];
    }

    public static void main(String[] args) {
        Queue queue = new Queue(3);

        queue.addQueue(1);
        queue.addQueue(2);
        queue.showFront();
        queue.showRear();
        queue.showQueue();

        int a = 4%3;
        System.out.println(a);
    }
}
