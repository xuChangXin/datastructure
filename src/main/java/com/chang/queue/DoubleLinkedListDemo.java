package com.chang.queue;

public class DoubleLinkedListDemo {

    public static void main(String[] args) {
        System.out.println("双向链表测试===========");
        DoubleLinkedHeroNode heroNode1 = new DoubleLinkedHeroNode(1, "宋江", "及时雨");
        DoubleLinkedHeroNode heroNode2 = new DoubleLinkedHeroNode(2, "李奎", "黑旋风");
        DoubleLinkedHeroNode heroNode3 = new DoubleLinkedHeroNode(3, "林冲", "豹子头");
        DoubleLinkedHeroNode heroNode4 = new DoubleLinkedHeroNode(4, "吴用", "智多星");

        DoubleLinkedList doubleLinkedList = new DoubleLinkedList();
//        doubleLinkedList.addHeroNode(heroNode1);
//        doubleLinkedList.addHeroNode(heroNode2);
//        doubleLinkedList.addHeroNode(heroNode3);
//        doubleLinkedList.addHeroNode(heroNode4);
        doubleLinkedList.addHeroNodeOrder(heroNode1);
        doubleLinkedList.addHeroNodeOrder(heroNode4);
        doubleLinkedList.addHeroNodeOrder(heroNode2);
        doubleLinkedList.addHeroNodeOrder(heroNode1);
        doubleLinkedList.showDoubleLinkedListNode();

        DoubleLinkedHeroNode heroNode5 = new DoubleLinkedHeroNode(4, "卢俊义", "玉麒麟");
        doubleLinkedList.updateHeroNode(heroNode5);
        System.out.println("修改双向链表");
        doubleLinkedList.showDoubleLinkedListNode();

        System.out.println("删除双向链表");
        DoubleLinkedHeroNode heroNode6 = new DoubleLinkedHeroNode(2, "卢俊义", "玉麒麟");
        doubleLinkedList.deleteNode(heroNode6);
        doubleLinkedList.showDoubleLinkedListNode();
    }
}

class DoubleLinkedList{

    private DoubleLinkedHeroNode headNode = new DoubleLinkedHeroNode(0,"","");

    DoubleLinkedHeroNode getHeroNode(){
        return headNode;
    }

    void addHeroNode(DoubleLinkedHeroNode doubleLinkedHeroNode){
        DoubleLinkedHeroNode node = headNode;
        while (node.next != null) {
            node = node.next;
        }
        node.next = doubleLinkedHeroNode;
        doubleLinkedHeroNode.prev = node;
    }

    void addHeroNodeOrder(DoubleLinkedHeroNode doubleLinkedHeroNode){
        DoubleLinkedHeroNode node = headNode;
        boolean status = false;
        while (node.next != null) {
            if (node.next.no > doubleLinkedHeroNode.no){
                node.next.prev = doubleLinkedHeroNode;
                doubleLinkedHeroNode.next = node.next;
                doubleLinkedHeroNode.prev = node;
                node.next = doubleLinkedHeroNode;
                status = true;
                break;
            }
            node = node.next;
        }
        if (!status) {
            node.next = doubleLinkedHeroNode;
            doubleLinkedHeroNode.prev = node;
        }
    }

    void updateHeroNode(DoubleLinkedHeroNode newHeroNode){
        if (headNode.next == null || newHeroNode == null){
            return;
        }

        DoubleLinkedHeroNode temp = headNode;
        while (true){
            if (temp.next == null){
                break;
            }
            if (temp.next.no.equals(newHeroNode.no)){
                temp.next.name = newHeroNode.name;
                temp.next.nickName = newHeroNode.nickName;
                break;
            }
            temp = temp.next;
        }
    }

    void showDoubleLinkedListNode(){
        if (headNode.next == null){
            return;
        }
        DoubleLinkedHeroNode temp = headNode;
        while (temp.next != null){
            System.out.println(temp.next);
            temp = temp.next;
        }
    }

    void deleteNode(DoubleLinkedHeroNode doubleLinkedHeroNode){
        if (headNode.next == null){
            return;
        }
        DoubleLinkedHeroNode temp = headNode.next;
        while (temp != null){
            if (temp.no.equals(doubleLinkedHeroNode.no)){
                temp.prev.next = temp.next;
                if (temp.next != null){
                    temp.next.prev = temp.prev;
                }
                break;
            }
            temp = temp.next;
        }
    }
}

class DoubleLinkedHeroNode{
    public Integer no;
    public String name;
    public String nickName;
    public DoubleLinkedHeroNode next;
    public DoubleLinkedHeroNode prev;

    public DoubleLinkedHeroNode(Integer no,String name,String nickName){
        this.no = no;
        this.name = name;
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "DoubleLinkedHeroNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", nickName='" + nickName + '\'' +
                '}';
    }
}