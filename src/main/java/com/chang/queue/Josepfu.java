package com.chang.queue;

public class Josepfu {

    public static void main(String[] args) {
        CircleSingleLinkedList circleSingleLinkedList = new CircleSingleLinkedList();
        circleSingleLinkedList.addBoy(5);
        circleSingleLinkedList.showCircleSingleLinkedList();

        circleSingleLinkedList.countBoy(2,3,5);
    }
}

class CircleSingleLinkedList{

    private Boy first = null;

    void addBoy(int nums){
        if (nums < 1){
            System.out.println("nums值不正确");
            return;
        }

        // 辅助变量
        Boy cur = null;
        for (int i = 1; i <= nums; i++) {
            // 创建对象
            Boy boy = new Boy(i);
            // 第一个 给头节点赋值
            if (i == 1){
                // 赋值给头节点 这样后续可以找到头节点
                first = boy;
                // 给该对象设置next域
                boy.setNext(first);
                cur = boy;
            }else {
                // cur 为最后一个结点 将最后一个结点的 next域设置为新的 boy结点
                cur.setNext(boy);
                // 再将新的boy结点的next域设置为头节点即可成环
                boy.setNext(first);
                // 再次将 新的boy赋值给 cur
                cur = boy;
            }
        }
    }
    void showCircleSingleLinkedList(){

        Boy temp = first;
        while (true){
            if (temp.getNext() == first){
                System.out.println("done");
                break;
            }
            System.out.println(temp);
            temp = temp.getNext();
        }
    }

    /**
     * 出圈
     * @param startNo  开始的编号
     * @param num      数的次数
     * @param countNum boy个数
     */
    void countBoy(int startNo,int num,int countNum) {
//
//        //辅助结点
//        Boy helper = first;
//
//        while (true){
//            if (helper.getNext() == first){
//                break;
//            }
//            helper = helper.getNext();
//        }
//
//        for (int i = 0; i < startNo - 1; i++) {
//            first = first.getNext();
//            helper = helper.getNext();
//        }
//
//
//        while (true) {
//            if (helper == first){
//                System.out.println("lastBoy="+helper.getNo());
//                break;
//            }
//            for (int i = 0; i < num - 1; i++) {
//                first = first.getNext();
//                helper = helper.getNext();
//            }
//            System.out.println("out "+first.getNo());
//            first = first.getNext();
//            helper.setNext(first);
//        }
//        System.out.println("all out");
//    }
        // 判断条件是否合规
        if (first == null || startNo < 1 || num < 1 || countNum < startNo){
            System.out.println("条件有误");
            return;
        }

        // 定义辅助指针 指向first上一个结点
        Boy helper = first;

        // 该循环 目的是帮助辅助变量 helper 找到first的上一个结点
        while (true){
            if (helper.getNext() == first){
                break;
            }
            helper = helper.getNext();
        }

        // startNo - 1 比如first 处于1   startNo为4  1到2  2到3  3到4 需要移动三次   如果不减1 会移动四次 i=3 first处于5的位置了
                                                // i=0   i=1  1=2
        for (int i = 0; i < startNo - 1; i++) {
            first = first.getNext();
            helper = helper.getNext();
        }

        while (true) {
            // 如果相等那么就剩一个了
            if (first == helper){
                System.out.println("last "+ first.getNo());
                break;
            }
            // 这个循环是进行喊数操作   num-1 因为从自身开始喊   比如 1 2 3  1开始喊 喊三次到3 但是只移动了两位（1到2 2到3）
            for (int i = 0; i < num - 1; i++) {
                first = first.getNext();
                helper = helper.getNext();
            }
            // 此时first 就是要出圈的boy
            System.out.println("out "+first.getNo());
            // 出圈操作
            // first 后移
            first = first.getNext();
            // 原first丢弃  helper指向新的first 就可断开原来的first结点 原有的first结点会被垃圾回收
            helper.setNext(first);
        }


    }
}

class Boy{
    private int no;
    private Boy next;

    public Boy(int no){
        this.no = no;
    }

    public int getNo() {
        return no;
    }

    public void setNext(Boy next) {
        this.next = next;
    }

    public Boy getNext() {
        return next;
    }

    @Override
    public String toString() {
        return "Boy{" +
                "no=" + no +
                '}';
    }
}
