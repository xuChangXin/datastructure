package com.chang.queue;

public class CircularQueue {
    // 最大容量
    private int maxSize;
    // 队列头
    private int front;
    // 队列尾
    private int rear;
    // 数据存放
    private int[] arr;

    public CircularQueue(int arrayMaxSize){
        maxSize = arrayMaxSize;
        arr = new int[maxSize];
        // 队列头部 指向队列头部的一个元素
        front = 0;
        // 队列尾部 指向队列的最后一个数据的后一个位置
        rear = 0;
    }

    /**
     * 判断队列是否满了
     * @return
     */
    public Boolean isFull(){
        return  (rear + 1) % maxSize == front;
    }

    /**
     * 判断队列为空
     * @return
     */
    public Boolean isEmpty(){
        return rear == front;
    }

    public void addQueue(int num){
        if (isFull()){
            System.out.println("队列已满,无法存入");
            return;
        }
        arr[rear] = num;
        rear = (rear + 1) % maxSize;
    }

    public int getQueue(){
        if (isEmpty()){
            System.out.println("暂无数据!");
            throw new RuntimeException("队列为空,暂无数据!");
        }
        int temp = arr[front];
        front = (front + 1) % maxSize;
        return temp;
    }

    public void showQueue(){
        if (isEmpty()){
            System.out.println("队列为空,暂无数据");
            return;
        }
        for (int i = front; i < front + (rear + maxSize - front) % maxSize; i++) {
            System.out.printf("array[%d]=[%d]\n",i % maxSize,arr[i % maxSize]);

        }
    }

    public int showFront(){
        if (isEmpty()){
            System.out.println("队列为空,暂无数据");
            throw new RuntimeException("队列为空,暂无数据!");
        }
        System.out.printf("front=%d\n",arr[front + 1]);
        return arr[front + 1];
    }

    public int showRear(){
        if (isEmpty()){
            System.out.println("队列为空,暂无数据");
            throw new RuntimeException("队列为空,暂无数据!");
        }
        System.out.printf("rear=%d\n",arr[rear]);
        return arr[rear];
    }

    public static void main(String[] args) {
        CircularQueue queue = new CircularQueue(3);

        queue.addQueue(1);
        queue.addQueue(2);
        queue.addQueue(3);
        queue.showQueue();
        queue.getQueue();
        queue.getQueue();
        queue.addQueue(4);
        queue.addQueue(5);
        queue.getQueue();
        queue.addQueue(6);
        queue.showQueue();
    }
}

