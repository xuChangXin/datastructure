package com.chang.sparseArray;

import java.io.*;
import java.net.URL;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * @author x
 * @date 2021/7/17
 */
public class Array2SparseArray {

    public static void method(){

        // 创建一个二维数组
        // 1 代表黑子 2 代表白子
        int[][] array = new int[11][11];

        array[1][2] = 1;
        array[2][3] = 2;
        array[3][5] = 2;

        int arrayLength = array.length;
        int array2Length = array[0].length;

        System.out.println("原始数组数据");
        int totalNum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.printf("%d\t",array[i][j]);
                if (array[i][j] != 0){
                    totalNum++;
                }
            }
            System.out.println();
        }

        System.out.println("有效元素个数"+totalNum+"===============================================================");

        // 创建原始数组
        int[][] sparsearray = new int[totalNum+1][3];
        sparsearray[0][0] = arrayLength;
        sparsearray[0][1] = array2Length;
        sparsearray[0][2] = totalNum;

        // 稀疏数组赋值
        // num记录第几个非零数据
        // 1 开始是因为已知第一行的数据 并且已经赋值了
        // 也可以0开始 但num++操作须在赋值之前
        int num = 1;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] != 0){
                    sparsearray[num][0] = i;
                    sparsearray[num][1] = j;
                    sparsearray[num][2] = array[i][j];
                    num++;
                }
            }
        }

        System.out.println("稀疏数组数据");

        long time = System.currentTimeMillis();

        File file = new File("D:\\IdeaProjects\\gitlab\\dataStructure2021\\src\\main\\resources\\"+time+".data");
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            for (int i = 0; i < sparsearray.length; i++) {
                for (int j = 0; j < sparsearray[i].length; j++) {
                    fileWriter.write(sparsearray[i][j] + "\t");
                    System.out.printf("%d\t",sparsearray[i][j]);
                }
                fileWriter.write("\r\n");
                System.out.println("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                assert fileWriter != null;
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        System.out.println("稀疏数组 恢复成原始二维数组");

        int[][] oldArray = new int[sparsearray[0][0]][sparsearray[0][1]];

        for (int i = 1; i < sparsearray.length; i++) {
            oldArray[sparsearray[i][0]][sparsearray[i][1]] = sparsearray[i][2];
        }

        System.out.println("原始数组的数据~~");

        for (int i = 0; i < oldArray.length; i++) {
            for (int j = 0; j < oldArray[i].length; j++) {
                System.out.printf("%d\t",oldArray[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        method();
    }
}
