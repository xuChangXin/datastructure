package com.chang.stack;

public class Stack {

    public static void main(String[] args) {
        // 数组模拟栈
//        ArrayStack arrayStack = new ArrayStack(10);
//        arrayStack.addStack(1);
//        arrayStack.addStack(2);
//        arrayStack.addStack(3);
//
//        arrayStack.list();
//        for (int i = 0; i < 3; i++) {
//            System.out.println(arrayStack.outStack());
//        }
//        arrayStack.addStack(4);
//        arrayStack.addStack(5);
//        arrayStack.addStack(6);
//        arrayStack.list();

        // 链表模拟栈
        LinkedListStackDemo linkedListStackDemo = new LinkedListStackDemo();
        LinkedListStack linkedListStack1 = new LinkedListStack();
        linkedListStack1.setNo(1);
        LinkedListStack linkedListStack2 = new LinkedListStack();
        linkedListStack2.setNo(2);
        LinkedListStack linkedListStack3 = new LinkedListStack();
        linkedListStack3.setNo(3);
        linkedListStackDemo.push(linkedListStack1);
        linkedListStackDemo.push(linkedListStack2);
        linkedListStackDemo.push(linkedListStack3);

        for (int i = 0; i < 3; i++) {
            LinkedListStack pop = linkedListStackDemo.pop();
            System.out.println(pop.getNo());
        }
    }

}


// 数组模拟栈
class ArrayStack{
    private int maxSize;
    private int top = -1;
    public int[] stack;

    public ArrayStack(int maxSize){
        this.maxSize = maxSize;
        stack = new int[maxSize];
    }

    // 栈满
    public boolean isFull(){
        return top == maxSize - 1;
    }

    // 栈空
    public boolean isEmpty(){
        return top == -1;
    }

    // 入栈
    public void addStack(int num){
        if (isFull()){
            System.out.println("Already MaxSize!");
            return;
        }
        top++;
        stack[top] = num;
    }

    // 出栈
    public int outStack(){
        if (isEmpty()){
            throw new RuntimeException("Stack isEmpty!");
        }
        int temp = stack[top];
        stack[top] = 0;
        top--;
        return temp;
    }

    // 遍历栈
    public void list(){
        if (isEmpty()){
            System.out.println("Stack isEmpty!");
            return;
        }
        for (int i = top; i >= 0; i--) {
            int value = stack[i];
            System.out.printf("stack[%d]=%d\n",i,value);
        }
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int[] getStack() {
        return stack;
    }

    public void setStack(int[] stack) {
        this.stack = stack;
    }
}
class LinkedListStackDemo{
    private LinkedListStack head = new LinkedListStack();

    void push(LinkedListStack linkedListStack){
        if (head.getNext() == null){
            head.setNext(linkedListStack);
            return;
        }

        LinkedListStack temp = head;
        // part 1
        while (true){
            if (temp.getNext() == null){
                temp.setNext(linkedListStack);
                break;
            }
            temp = temp.getNext();
        }

        // part 2
//        LinkedListStack temp = head.getNext();
//
//        linkedListStack.setNext(temp);
//
//        head.setNext(linkedListStack);
    }


    // 链表模拟栈
    LinkedListStack pop(){
        if (head.getNext() == null){
            throw new RuntimeException("LinkedListStack isEmpty!");
        }

        LinkedListStack temp = head.getNext();
        LinkedListStack temp2 = head;
        while (true){
            if (temp.getNext() == null){
                temp2.setNext(null);
                return temp;
            }
            temp2 = temp;
            temp = temp.getNext();
        }
    }
}


class LinkedListStack{
    private int no;
    private LinkedListStack next;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public LinkedListStack getNext() {
        return next;
    }

    public void setNext(LinkedListStack next) {
        this.next = next;
    }
}
