package com.chang.stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class PolandNotation {

    public static void main(String[] args) {

        String expression = "1+((2+3)*4)-5";

        // 获取中缀表达式的数组形式
        List<String> expressionList = toInfixExpressionList(expression);
        System.out.println(expressionList);
        // 中缀表达式转为后缀表达式
        // 运算符栈
        Stack<String> s1 = new Stack<>();
        // 中间结果栈
        List<String> s2 = new ArrayList<>();

        for (String item : expressionList) {
            if (item.matches("\\d+")){ // 判断是数字
                s2.add(item);
            }else if (item.equals("(")){
                s1.push(item);
            }else if (item.equals(")")){
                while (!s1.peek().equals("(")){
                    s2.add(s1.pop());
                }
                // 弹出左括号
                s1.pop();
            }else {
                while (s1.size() != 0 && operatorLevel(s1.peek()) >= operatorLevel(item)){
                    s2.add(s1.pop());
                }
                s1.push(item);
            }
        }

        while (s1.size() != 0){
            s2.add(s1.pop());
        }
        System.out.println(s2);
    }

    private static int operatorLevel(String operator){
        int result = 0;
        switch (operator){
            case "+":
            case "-":
                result = 1;
                break;
            case "*":
            case "/":
                result = 2;
                break;
            default:
                break;
        }
        return  result;
    }

    private static List<String> toInfixExpressionList(String expression) {

        List<String> ls = new ArrayList<>();
        int index = 0;
        StringBuilder temp;
        char c;

        while (index < expression.length()){
            if ((c=expression.charAt(index)) < 48 || (c=expression.charAt(index)) > 57){
                ls.add(String.valueOf(c));
                index++;
            }else {
                temp = new StringBuilder();
                while (index < expression.length() && (c=expression.charAt(index)) >= 48 &&  (c=expression.charAt(index)) <= 57){
                    temp.append(c);
                    index++;
                }
                ls.add(temp.toString());
            }
        }
        return ls;
    }


}
