package com.chang.stack;

public class Calc {


    public static void main(String[] args) {
        ArrayStackCalc arrayStackCalcNumber = new ArrayStackCalc(10); // 数栈
        ArrayStackCalc arrayStackCalcOperator = new ArrayStackCalc(10); // 符号栈
        String expression = "200+3*4*50";
        // 扫描字符串的索引标识
        int index = 0;

        String keepNum = "";
        while (true){
            // 获取到一个字符
            char c = expression.substring(index, index + 1).charAt(0);
            // 如果不是运算符
            if (!arrayStackCalcNumber.isOperator(c)){
                keepNum += c;
                // ASCII码表 相差48
//                arrayStackCalcNumber.addStack(c - 48);
                if (index == expression.length() - 1){ // 判断是不是最后一个
                    arrayStackCalcNumber.addStack(Integer.parseInt(keepNum));
                }else {
                    // 往表达式后一位 查看是不是运算符 如果是就入栈 然后清空keepNum temp
                    if (arrayStackCalcNumber.isOperator(expression.substring(index+1,index+2).charAt(0))){
                        arrayStackCalcNumber.addStack(Integer.parseInt(keepNum));
                        keepNum = "";
                    }
                }
            }else { // 是运算符
                if (arrayStackCalcOperator.isEmpty()){ // 为空直接入栈
                    arrayStackCalcOperator.addStack(c);
                }else {
                    // 判断符号优先级
                    if (arrayStackCalcOperator.priority(c) <= arrayStackCalcOperator.priority(arrayStackCalcOperator.pick())){
                        // 出栈两个数字 一个符号 进行运算
                        int numberOne = arrayStackCalcNumber.outStack();
                        int numberTwo = arrayStackCalcNumber.outStack();
                        int operator = arrayStackCalcOperator.outStack();
                        // 计算
                        int cal = arrayStackCalcNumber.cal(numberOne, numberTwo, operator);
                        // 将运算结果入数栈
                        arrayStackCalcNumber.addStack(cal);
                        // 符号入符号栈
                        arrayStackCalcOperator.addStack(c);
                    }else {
                        arrayStackCalcOperator.addStack(c);
                    }
                }
            }
            index++;
            if (index >= expression.length()){
                break;
            }
        }

        int result = 0;
        while (!arrayStackCalcOperator.isEmpty()) {
            int i1 = arrayStackCalcNumber.outStack();
            int i2 = arrayStackCalcNumber.outStack();
            int i3 = arrayStackCalcOperator.outStack();
            result = arrayStackCalcNumber.cal(i1, i2, i3);
            arrayStackCalcNumber.addStack(result);
        }
        System.out.printf("%s = %d",expression,arrayStackCalcNumber.outStack());

    }
}


// 数组模拟栈
class ArrayStackCalc{
    private int maxSize;
    private int top = -1;
    public int[] stack;

    public ArrayStackCalc(int maxSize){
        this.maxSize = maxSize;
        stack = new int[maxSize];
    }

    // 栈满
    public boolean isFull(){
        return top == maxSize - 1;
    }

    // 栈空
    public boolean isEmpty(){
        return top == -1;
    }

    // 入栈
    public void addStack(int num){
        if (isFull()){
            System.out.println("Already MaxSize!");
            return;
        }
        top++;
        stack[top] = num;
    }

    // 出栈
    public int outStack(){
        if (isEmpty()){
            throw new RuntimeException("Stack isEmpty!");
        }
        int temp = stack[top];
        stack[top] = 0;
        top--;
        return temp;
    }

    // 遍历栈
    public void list(){
        if (isEmpty()){
            System.out.println("Stack isEmpty!");
            return;
        }
        for (int i = top; i >= 0; i--) {
            int value = stack[i];
            System.out.printf("stack[%d]=%d\n",i,value);
        }
    }

    // 返回当前栈顶的数据 不是出栈仅作展示
    public int pick(){
        return stack[top];
    }

    // 判断运算符的优先级  数字越大优先级越高
    public int priority(int operator){
        if (operator == '*' || operator == '/'){
            return 1;
        }else if (operator == '+' || operator == '-'){
            return 0;
        }else {
            return -1;
        }
    }

    // 判断 是否是运算符
    public boolean isOperator(char operator){
        return operator == '+' || operator == '-' || operator == '*' || operator == '/';
    }

    // 运算操作
    public int cal(int numOne,int numTow,int operator){
        int result = 0;
        switch (operator){
            case '+':
                result = numOne + numTow;
                break;
            case '-':
                result = numTow - numOne;
                break;
            case '*':
                result = numOne * numTow;
                break;
            case '/':
                result = numTow / numOne;
                break;
            default:
                break;
        }
        return result;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int[] getStack() {
        return stack;
    }

    public void setStack(int[] stack) {
        this.stack = stack;
    }
}