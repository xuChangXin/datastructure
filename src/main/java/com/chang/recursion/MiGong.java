package com.chang.recursion;

public class MiGong {

    public static void main(String[] args) {
        // 定义一个二维数组
        int[][] miGong = new int[8][8];

        for (int i = 0; i < 8; i++) {
            miGong[0][i] = 1;
            miGong[7][i] = 1;
        }
        for (int i = 0; i < 8; i++) {
            miGong[i][0] = 1;
            miGong[i][7] = 1;
        }

        miGong[3][1] = 1;
        miGong[3][2] = 1;
        miGong[1][2] = 1;
        miGong[2][2] = 1;

        findWay(miGong,1,1);

        for (int[] ints : miGong) {
            for (int anInt : ints) {
                System.out.printf("%d ", anInt);
            }
            System.out.println();
        }

        System.out.println(num);

    }
     static int num = 0;

    /**
     * 递归回溯
     * @param param 地图
     * @param i 表示哪个位置开始找  开始位置(1,1)  目标位置(6,6)
     * @param j
     *
     * rules: [i][j]为0时 说明路没走过 为1时表示墙 2表示通路 3表示已经走过，但是走不通
     *         策略：下-右-上-左,如果走不通 再回溯
     * @return
     */
    public static boolean findWay(int[][] param,int i ,int j){
        if (param[6][6] == 2){
            return true;
        }else { // 下 右 上 左
            if (param[i][j] == 0){
                param[i][j] = 2;
                if (findWay(param,i+1,j)){
                    num++;
                    return true;
                }else if (findWay(param,i,j+1)){
                    num++;
                    return true;
                }else if (findWay(param,i-1,j)){
                    num++;
                    return true;
                }else if (findWay(param,i,j-1)){
                    num++;
                    return true;
                }else {
                    param[i][j] = 3;
                    return false;
                }
            }else {
                return false;
            }
        }
    }
}
